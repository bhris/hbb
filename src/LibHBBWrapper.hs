{-# LANGUAGE DeriveDataTypeable #-}

module LibHBBWrapper (
    hbb_locate,
    hbb_inline,
    hbb_smartinline,
    hbb_applyto,
    hbb_occursof,
    hbb_exprtype,
    HBBOptions(..),
    defaultHBBOptions,
    GHCModError(..)) where

import           Language.Haskell.HBB.OccurrencesOf
import           Language.Haskell.HBB.SmartInline
import           Language.Haskell.HBB.ExprType
import           Language.Haskell.HBB.ApplyTo
import           Language.Haskell.HBB.Inline
import           Language.Haskell.HBB.Locate
import           Language.Haskell.GhcMod
import           System.Console.GetOpt (OptDescr(..), ArgDescr(..), ArgOrder(..))
import qualified System.Console.GetOpt as O
import           Control.Applicative ((<$>))
import           System.Environment (getArgs)
import           Control.Exception (SomeException,Exception, Handler(..), ErrorCall(..))
import qualified Control.Exception as E
import           System.Directory (doesFileExist)
import           Data.Typeable (Typeable)
import           Data.Version (showVersion)
import           System.Exit (exitFailure)
import           System.IO (hPutStr, hPutStrLn, stdout, stderr, hSetEncoding, utf8)
import           CoreMonad (liftIO)
import           Paths_hbb
import           Config (cProjectVersion)
import           GHC (GhcMonad,gcatch)

----------------------------------------------------------------

data GHCModError = SafeList
                 | ArgumentsMismatch String
                 | NoSuchCommand String
                 | CmdArg [String]
                 | FileNotExist String deriving (Show, Typeable)

instance Exception GHCModError

----------------------------------------------------------------

data HBBOptions = HBBOptions { adaptInd     :: NonFirstLinesIndenting
                             , quietApplyTo :: Bool }

defaultHBBOptions :: HBBOptions
defaultHBBOptions = HBBOptions { adaptInd     = IgnoreIndOfTargetEnv
                               , quietApplyTo = False }

hbb_locate :: IOish m => FilePath -> Int -> Int -> GhcModT m String
hbb_locate filename line col = do
    result <- do
        loc <- locateM filename (BufLoc line col)
        return $ (showLocateResult loc) ++ "\n"
    return result

hbb_inline :: IOish m => HBBOptions -> FilePath -> Int -> Int -> Maybe Int -> Maybe Int -> GhcModT m String
hbb_inline hbbopts filename line1 col1 (Just line2) (Just col2) = do
    (_,result) <- inlineM (defaultInlineOptions { adaptToTargetEnv = (adaptInd hbbopts) }) filename (BufLoc line1 col1) (Just $ BufLoc line2 col2)
    return (result ++ "\n")
hbb_inline hbbopts filename line1 col1 Nothing      Nothing     = do
    (_,result) <- inlineM (defaultInlineOptions { adaptToTargetEnv = (adaptInd hbbopts) }) filename (BufLoc line1 col1) Nothing
    return (result ++ "\n")
hbb_inline _ _ _ _ _ _ = E.throw $ ArgumentsMismatch "no second column number"


hbb_smartinline :: IOish m => HBBOptions -> FilePath -> Int -> Int -> Maybe Int -> Maybe Int -> GhcModT m String
hbb_smartinline hbbopts filename line1 col1 (Just line2) (Just col2) = do
    result <- smartinlineM (adaptInd hbbopts) filename (BufLoc line1 col1) (Just $ BufLoc line2 col2)
    return $ showSmartInlineResult result
hbb_smartinline hbbopts filename line1 col1 Nothing      Nothing     = do
    result <- smartinlineM (adaptInd hbbopts) filename (BufLoc line1 col1) Nothing
    return $ showSmartInlineResult result
hbb_smartinline _ _ _ _ _ _ = E.throw $ ArgumentsMismatch "no second column number"

hbb_occursof :: IOish m => FilePath -> Int -> Int -> [FilePath] -> GhcModT m String
hbb_occursof filename line1 col1 otherFiles = do
    result <- occurrencesOfM filename (BufLoc line1 col1) otherFiles
    return $ showOccurrencesOfResult result

hbb_exprtype :: IOish m => FilePath -> String -> GhcModT m String
hbb_exprtype filename exprStr = do
    result <- exprtypeM filename exprStr
    return $ (showExprTypeResult result ++ "\n")

hbb_applyto :: IOish m => HBBOptions -> String -> String -> GhcModT m String
hbb_applyto hbbopts fun subj = do
    (okMsgs,errMsg) <- liftIO $ applyTo (quietApplyTo hbbopts) fun subj
    case errMsg of
        Nothing -> return ()
        Just ms -> do liftIO $ putStr ms
                      liftIO $ putStrLn "> "
                      liftIO $ putStrLn "> Use the '-q' flag to suppress all messages to standard error (like this one)"
    return okMsgs
