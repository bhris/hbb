#!/bin/bash
#
# This is a shell script intended to speedup the edit-compile cycle. It uses
# intofiy (from "inotify-tools" under Debian) to monitor file changes and
# triggers a recompile as soon as changes have been detected.

which inotifywait >/dev/null 2>/dev/null || {
    echo "Error: The command inotifywait is needed to use this script!"
}

while inotifywait -q -q -e close_write ./src; do
    cabal build
done
