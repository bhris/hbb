# **hbb**: Extraordinary Haskell programming

This project aims to create a tool which should be easily embeddable in text
editors to assist them to provide extraordinary editing features for the
Haskell programming language. To archieve this, the tool will is based on the
library of the Glasgow Haskell Compiler (GHC) and the tool *ghc-mod* which
already improves text editors with a certain number of features.

The name **hbb** is short for *h*askell *b*usy *b*ee and should remind one of
the programmers using it.

The central library of *hbb* has been split apart into its own cabal package
which has the name *libhbb*. This library also ships an executable (named
*libhbb-cli*) which has reduced functionality compared to *hbb*. *hbb* is able
to provide much more functionality because it builds upon the library of
*ghc-mod*. Altogether the following dependencies materialize:

              +---------+
              |   hbb   |
              +---------+
               | (exe) |
               |       |
               v       v
      +---------+     +--------+
      | ghc-mod |     | libhbb |
      +---------+     +--------+
      (lib & exe)     (lib & exe)

*hbb* as well as *ghc-mod* (as well as *libhbb-cli*) have different mode of
operations which are dictated by a command line parameter. The target of
*hbb* is it to extend *ghc-mod* which has currently (as of 2014-09-23) a total
of 16 modes (including things like *help*) by certain things.

One outstanding feature *hbb* wants to add is the ability to replace the
occurence of a function name by an inlined version of its body (its lambda
representation).

# Extract from the *hbb help*

    hbb version 0.4.0.0 compiled by GHC 7.6.3
    Usage:
      hbb list [-g GHC_opt1 -g GHC_opt2 ...] [-l] [-d]
      hbb lang [-l]
      hbb flag [-l]
      hbb browse [-g GHC_opt1 -g GHC_opt2 ...] [-l] [-o] [-d] [-q] [<package>:]<module> [[<package>:]<module> ...]
      hbb check  [-g GHC_opt1 -g GHC_opt2 ...] <HaskellFiles...>
      hbb expand [-g GHC_opt1 -g GHC_opt2 ...] <HaskellFiles...>
      hbb debug  [-g GHC_opt1 -g GHC_opt2 ...] 
      hbb info   [-g GHC_opt1 -g GHC_opt2 ...] <HaskellFile> <module> <expression>
      hbb type   [-g GHC_opt1 -g GHC_opt2 ...] <HaskellFile> <module> <line-no> <column-no>
      hbb split  [-g GHC_opt1 -g GHC_opt2 ...] <HaskellFile> <module> <line-no> <column-no>
      hbb sig    [-g GHC_opt1 -g GHC_opt2 ...] <HaskellFile> <module> <line-no> <column-no>
      hbb refine [-g GHC_opt1 -g GHC_opt2 ...] <HaskellFile> <module> <line-no> <column-no> <expression>
      hbb auto   [-g GHC_opt1 -g GHC_opt2 ...] <HaskellFile> <module> <line-no> <column-no>
      hbb find <symbol>
      hbb lint [-h opt] <HaskellFile>
      ---------- modes supported by libhbb ------------
      hbb locate         [-g GHC_opt1 -g GHC_opt2 ...]               <HaskellFile> <line-no> <column-no>
      hbb inline         [-g GHC_opt1 -g GHC_opt2 ...] [--adapt-ind] <HaskellFile> <line-no> <column-no> [<line-no> <column-no>]
      hbb smart-inline   [-g GHC_opt1 -g GHC_opt2 ...] [--adapt-ind] <HaskellFile> <line-no> <column-no> [<line-no> <column-no>]
      hbb occurrences-of [-g GHC_opt1 -g GHC_opt2 ...]               <HaskellFile> <line-no> <column-no> [<HaskellFile> ...]
      hbb exprtype       [-g GHC_opt1 -g GHC_opt2 ...]               <HaskellFile> <expression>
      hbb apply-to       [-g GHC_opt1 -g GHC_opt2 ...] [-q] <Function of String to String> <Subject of String>
      --- end of modes supported by libhbb ------------
      hbb root
      hbb doc <module>
      hbb boot
      hbb version
      hbb help

    <module> for "info" and "type" is not used, anything is OK.
    It is necessary to maintain backward compatibility.

      -l           --tolisp              print as a list of Lisp
      -h hlintOpt  --hlintOpt=hlintOpt   hlint options
      -g ghcOpt    --ghcOpt=ghcOpt       GHC options
      -v           --verbose             verbose
      -o           --operators           print operators, too
      -d           --detailed            print detailed info
      -q           --qualified, --quiet  show qualified names / suppress warning (quiet) in mode apply-to
      -b sep       --boundary=sep        specify line separator (default is Nul string)
                   --adapt-ind           Adapt the indentation of non-first lines to the (possibly higher) ind of the target environment
