
Version 0.4.0.2
---------------

 - Adding CHANGELOG.md and README.md to the cabal package.

Version 0.4.0.0
---------------
 - Vim plugin is how shipped (only for mode inline)
 - The mode inline is enabled to omit the leading 'case' if not needed.
 - Integrating the test mode apply-ttree.
 - Where clauses can now be converted to let-expressions (in mode inline and
   smart-inline). This is only done if this provides the possibility to leave
   out a leading 'case' expression.
    
