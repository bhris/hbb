
:let g:hbb_executable_name     = "hbb"

" ghc options must be written in the same way they are passed to hbb which
" means that each option must be prepended by '-g'. For example
" let g:hbb_ghc_options = '-g -isrc -g -packageghc -g -XFlexibleInstances'
:let g:hbb_ghc_options         = ""

" This function inlines the currently selected function or prints an error
" message if inlining fails.
" If any argument is passed then the file is saved after inlining.
function! HBBInlineSelection( ... )
    :let curline  = getline('.')
    :let startpos = getpos( "'<" )
    :let endpos   = getpos( "'>" )
    :let curfile  = expand('%')
    :let commandstr  = g:hbb_executable_name . " " . g:hbb_ghc_options . " inline --adapt-ind " . curfile . " " . startpos[1] . " " . startpos[2] . " " . endpos[1] . " " . (endpos[2] + 1) . " 2>./.vim-hbb.log"
    :let fun         = system( commandstr )
    :let errorreason = readfile( "./.vim-hbb.log" ) 
    :call delete("./.vim-hbb.log")
    :if len( fun ) == 0
        :let errorreason_copy = copy( errorreason )
        :let errorreason_copy[0] = "Inlining failed: " . errorreason[0]
        :echo join( errorreason_copy , "\n" )
    :else
        :let funnamelen = endpos[2] - startpos[2] + 1

        :if startpos[2] < 2
            :let curlineprefix = ""
        :else
            :let curlineprefix = curline[0:(startpos[2]-2)]
        :endif
        :let curlinesuffix = curline[startpos[2]+funnamelen-1:]
        :let content2insertAsList = split( fun , '\n' )
        :let firstlineEnd   = content2insertAsList[0]
        :let  lastlineStart = content2insertAsList[-1]
        :if len( content2insertAsList ) >= 2
            " This is a multiline entry...
            :call remove( content2insertAsList ,  0 )
            :call remove( content2insertAsList , len( content2insertAsList ) - 1 )
            :call insert( content2insertAsList , curlineprefix . firstlineEnd )
            :call add( content2insertAsList    , lastlineStart . curlinesuffix )
        :else
            " This is a single-line entry...
            :let content2insertAsList[0] = curlineprefix . content2insertAsList[0] . curlinesuffix
        :endif
        :call setline(     '.'  , content2insertAsList[0]  )
        :call append( line('.') , content2insertAsList[1:] )

        :if len( a:000 ) != 0
            :w
        :endif

    :endif
    :call setpos( '.' , startpos )
endfunction

":vmap i :call HBBInlineSelection()<CR>
":vmap I :call HBBInlineSelection( "with saving" )<CR>
